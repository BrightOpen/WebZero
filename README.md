# WebZero manifest

You've heard about Web 2.0, essentially a vision that the web is created by the community of users who are enabled to participate with minimal technical requirements. That's all well and candy. What this movement omited is a solution for privacy and security. Here we go. 

A little story for illustration... Imagine walking into a restaurant you've never been to before. There is plenty of space but all chairs and tables are taken. You ask the waiter and he assures you: "Just go fetch a table and chair from the furniture store next door." Somewhat bevildered, you comply. Ok now you're seated and you ask for the menu. The waiter goes: "Aha you haven't printed it yet? Here is the link and there are a couple of print shops around." Now a little bit weary, you're ready to order. The waiter is demanding payment upfront: "We only accept wire transfers. Please go to your bank and with this reference number pay your bill to our account." You start boiling but heck, it wouldn't be you to give up now. The waiter's smile greets you when you return from the bank: "We have received your payment, thank you! Here's a voucher for the takeaway across the street, please pick up your meal and bring it over here." You start to develop an irresistible tick in your eye. But at last, you're seated and the meal is in front of you. You got the point now: go home and bring the plate and cutlery.

I would not be coming back to such a service, would you? Yet this is exactly what we do online every day. Hiden behind technology, the interconnected web makes your browser run to a number of different providers to deliver a single service. CDNs here, shared pictures there, ads, tracking, authentication, payment providers and what not APIs... This all leads to leakage of private information to third parties you as an every day user are not aware of. With such a status quo, you're left with little choice: succumb, walk away or start hacking.

WebZero aims to disrupt this status quo. Imagine we could start from scratch again, where would you start?

## What is WebZero?

It is a call to action to bring back the basics and the trust into an online experience. It is a reset of web design. Let us start with a couple of zeros:

 * **Zero cross site access** - All resources must be served from the same origin, same domain. That means no third party CDNs, no cross linked images, no embeded content or ads from different providers, no APIs that are not served by the same origin. The only point where you reach another site is when you consciously click a link (and optionally confirm the site switch) and even that without a referer trace.
 * **Zero assumptions** about the receiving user, browser or device capabilities - Basic HTML with decent usability in text/mobile browsers without Javascript or CSS is served first. The service must function without the state of the art HTML5 widgets. If javascript or CSS is allowed, then upgrade gracefully.
 * **Zero confusion** - The intent of the service, code, links and controls must be absolutely clear. Links to other sites should only contain the bare minimum required data that can easily be inspected (and possibly modified) by the user prior to making the request. Forms should be just forms, they POST to the same page they are served from.
 * **Zero plaintext** - All information is encrypted in transit. Due to the 'zero cross site access' policy, there should always be only one certificate active at one time for a given site. The validity of site certificate can be easily inspected by the user. Even better, user should have maximum control over which certificates are accepted.
 * **Zero tracking** - Especially not without users explicit consent, not by default. No unwaranted user activity logs.

## Why does it matter?

We lost track of who we are talking to when we go online. We have handed over the responsibility for our security, because we don't understand it or perhasp because it's too much effort. If security was that much accessible that a kid could work it out on a whim, we'll be able to step up and own it.

Many companies harvest our personal data, analyse our behavioral paterns, and target us. Not only to sell us the best matching product as they claim (which is annoying enough), but also because we are apparently the potential terrorists, if you can believe it (see prism). 

Such surveilance has been and will be used to repress political and commercial dissent and has close to no value in uncovering well organized threats. In the current situation, it is difficult to hold one provider accountable. It is near to impossible to avoid the spying and tracking if you use the web like the usual visitor.

WebZero can change that.

## How can I participate?

 * **Promote this manifest** - Talk about it, share it, link to it. Demand it from your service providers.
 * **Implement WebZero Services** - Earn trust and praise by following the WebZero principles.
 * **Implement WebZero Browsers** - Add WebZero mode to web browsers, improve and extend same origin policy extensions and other privacy improvements.

### Specific actions?

 * Web browser developers:
  * Announce WebZero requirement to the server - X-WebZero: required.
  * Announce Do-not-track to the server - DNT: 1.
  * Inform user whether WebZero mode is enabled or not. Enable automatically when supported by server.
  * Warn user about leaving the site boundary.
  * Let user decide whether to follow or not.
  * Let user inspect and modify data shared between sites.
  * Always check https (encrypted channel) first.
  * Do not send referrer to another site. Or do not send referrer at all.
  * Disable cross site access. Report violations.
  * Support the user in validating certificates conveniently, support alternative validation methods (web of trust, fingerprint whitelist...), detect certificate change - locally and anonymously.
  * Give users convenient control over content stored per site (cookies, local storage, history) such as wiping storage per site with one click, setting retention policy per site, inspecting site stored content at a glance, privacy modes... 
 * Web service providers:
  * Announce WebZero support to the browser. X-WebZero: supported.
  * Comply with WebZero requirement.
  * Comply with Do-not-track - DNT: 1.
  * Proxy any and all offsite resources - yes it costs money but who said trust is cheap?
  * Close port 80, and only accept encrypted traffic.
  * Use proven and up to date cryptography.
  * Back to basics, lean, easy to understand cross site links.
  * If you need to share complex data with another site, make a server side call.
  * Plain simple HTML first, upgrade if supported, responsive and accessible design.
  * Test UX with simple text browsers as primary target (noJS, noCSS, basic HTML).
  * Avoid cookies. Use short HttpOnly Secure SameSite Session Cookie for authentication purposes if necessary.
 * Users, communities and institutions:
  * File bugs and feature requests to bring more of WebZero into daily life.
  * Make WebZero a requirement of your shopping / procurement / tendering.
  * Implement policies and regulations favoring and demanding WebZero.
 * Engineers
  * Tune standards for better privacy and control.
  * Deliver new concepts for anonymous exchange.
  * Make security and privacy more accessible to everyday user.
  * Abandon flawed legacy standards and systems.
 
Let's move on!

With love,
Robert Cutajar

Published under the 'Attribution-ShareAlike 4.0 International' license.
Copyright 2017 Robert Cutajar

